import { useState } from "react"
import Button from "../Button"
import ModalText from "../Modal/ModalText"
import ModalImage from "../Modal/ModalImage"
import Modal from "../Modal/Modal"
import ModalWrapper from "../Modal/ModalWrapper"
import ModalHeader from "../Modal/ModalHeader"
import ModalFooter from "../Modal/ModalFooter"
import ModalClose from "../Modal/ModalClose"
import ModalBody from "../Modal/ModalBody"


function ModalPage({title, type, click, text, secondaryText}) {
  const [modalState, setModalState] = useState({
    isOpen: false,
    type: "",
  });

  const openModal = (type) => {
    setModalState({ isOpen: true, type });
  };

  const closeModal = () => {
    setModalState({ isOpen: false, type: "" });
  };
  
  return (
    <>
      <Button
        classNames="button_modal"
        title={title}
        onClick={() => openModal(type)}
      />

      {modalState.isOpen && (
        <ModalWrapper onClose={closeModal}>
          <Modal>
            {modalState.type === "first" && (
              <>
                <ModalHeader>
                  <ModalImage />
                </ModalHeader>
                <ModalClose onClick={closeModal} />
                <ModalBody>
                  <ModalText
                    title={title}
                    text={text}
                  />
                </ModalBody>
                <ModalFooter
                  firstText="NO, CANCEL"
                  secondaryText={secondaryText}
                  firstClick={closeModal}
                  secondaryClick={click}
                />
              </>
            )}
            {modalState.type === "second" && (
              <>
                <ModalClose onClick={closeModal} />
                <ModalBody>
                  <ModalText
                    title='Add Product “NAME”'
                    text="Description for your product"
                  />
                </ModalBody>
                <ModalFooter firstText="ADD TO FAVORITE" firstClick={closeModal} />
              </>
            )}
          </Modal>
        </ModalWrapper>
      )}
    </>
  );
}

export default ModalPage;